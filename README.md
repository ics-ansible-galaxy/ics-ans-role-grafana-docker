# ics-ans-role-grafana-docker

Ansible role to install grafana-docker.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
# grafana
grafana_protocol: http # http, https or socket
grafana_port: 3000
grafana_provider: file
grafana_smtp_host: smtpserver # hostname or ipaddress
grafana_smtp: true # true or false

# ldap.toml for grafana
ldap_host: ldapserver # hostname or ipaddress
ldap_port: 636
ldap_ssl: true # true or false 
ldap_base_dn: "dc=local,dc=com"
ldap_bind_dn: "cn=Manager,{{ ldap_base_dn }}"
ldap_bind_password: somepassword
ldap_admin_group: "cn=admin,{{ ldap_base_dn }}"
ldap_group_dn: "cn=users,{{ ldap_base_dn }}"
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-grafana-docker
```

## License

BSD 2-clause
