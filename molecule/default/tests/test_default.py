import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_container(host):
    with host.sudo():
        for container_name in ['grafana', 'grafana-oncall', 'grafana-oncall-celery']:
            container = host.docker(container_name)
            assert container.is_running


def test_api(host):
    cmd = host.run('curl -k -f https://ics-ans-role-grafana-docker-default/api/health')
    assert cmd.rc == 0
